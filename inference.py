import pandas as pd
import mlflow
import os
from dotenv import load_dotenv
import uvicorn
from fastapi import FastAPI, File, UploadFile, HTTPException
from fastapi.responses import StreamingResponse
from prometheus_fastapi_instrumentator import Instrumentator
from prometheus_client import Gauge

from utils import parse_df, setup, build_df, TrackNETInference


# Load the environment variables from the .env file into the application
load_dotenv()

# Initialize the FastAPI application
app = FastAPI()

# Initialize Prometheus endpoint
Instrumentator().instrument(app).expose(app)

# Create a class to store the deployed model & use it for prediction
class Model:
    def __init__(self, model_name):
        """
        To initialize the model
        model_name: Name of the model in registry
        model_stage: Stage of the model
        """
        # Load the model from Registry
        model = mlflow.pyfunc.load_model(f"models:/{model_name}/latest")
        self.model = TrackNETInference(model)

    def predict(self, data):
        """
        To use the loaded model to make predictions on the data
        data: Pandas DataFrame to perform predictions
        """
        numpy_data = parse_df(data)
        inputs = setup(*numpy_data)
        preds = self.model(*inputs)
        output_df = build_df(preds)
        return output_df


# Create model
model = Model("tracknet")

columns = ['event', 'x', 'y', 'z', 'station', 'track', 'px', 'py', 'pz', 'vtxx', 'vtxy', 'vtxz']

# Create the POST endpoint with path '/invocations'
@app.post("/invocations")
async def create_upload_file(file: UploadFile = File(...)):
    if file.filename.endswith(".tsv"):
        # Create a temporary file with the same name as the uploaded
        # CSV file to load the data into a pandas Dataframe
        with open(file.filename, "wb") as f:
            f.write(file.file.read())
        data = pd.read_csv(file.filename, sep="\t", names=columns)
        os.remove(
            file.filename
        )

        result_df = model.predict(data)

        output = result_df.to_csv(index=False)
        return StreamingResponse(
            iter([output]),
            media_type="text/csv",
            headers={
                "Content-Disposition": "attachment;filename=<file_name>.csv"
            },
        )

    else:
        # Raise a HTTP 400 Exception, indicating Bad Request
        # (you can learn more about HTTP response status codes here)
        raise HTTPException(
            status_code=400,
            detail="Invalid file format. Only TSV Files accepted.",
        )


# Check if the environment variables for AWS access are available.
# If not, exit the program
if (
    os.getenv("AWS_ACCESS_KEY_ID") is None
    or os.getenv("AWS_SECRET_ACCESS_KEY") is None
):
    exit(1)


if __name__ == "__main__":
    uvicorn.run("inference:app", host="0.0.0.0", port=8000, reload=True)
