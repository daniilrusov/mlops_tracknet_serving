FROM python:3.10.12

#
WORKDIR /code

#
RUN pip install --upgrade pip

COPY ./requirements.txt /code/

COPY ./inference.py /code/inference.py
COPY ./utils.py /code/utils.py
COPY ./.env /code/.env

ENV TMPDIR='./tmp'

RUN pip install -r requirements.txt
  
  
CMD ["python", "inference.py"]


