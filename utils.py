import torch
import pandas as pd
from typing import List

N_STATIONS = 35
DEVICE = 'cpu'


class TrackNETInference(torch.nn.Module):
    def __init__(self, model):
        super().__init__()
        self.model = model

    def forward(
        self,
        chunk_data_x,
        cand_mask,
        hits_indexes_global,
        hits_by_station: List[torch.Tensor],
        indexes_by_station: List[torch.Tensor],
    ):
        chunk_data_x = chunk_data_x.clone()
        cand_mask = cand_mask.clone()
        hits_indexes_global = hits_indexes_global.clone()
        max_n_stations = 35

        prediction = self.exec_model(chunk_data_x, 1)
        (
            first_dists,
            second_dists,
            first_nearest_hits,
            second_nearest_hits,
            first_hits_indexes,
            second_hits_indexes,
        ) = self.search_both(
            prediction, hits_by_station[1], indexes_by_station[1]
        )
        in_ellipse_mask1 = self.filter_hits_by_distance(prediction,
                                                        first_dists)
        in_ellipse_mask2 = self.filter_hits_by_distance(
            prediction[:, 4:], second_dists
        )

        saved_mask = (in_ellipse_mask1 == 0) * (in_ellipse_mask2 == 1)
        saved_hits = second_nearest_hits
        saved_indexes = second_hits_indexes

        chunk_data_x, hits_indexes_global, cand_mask = self.prolong(
            in_ellipse_mask1,
            in_ellipse_mask2,
            chunk_data_x,
            1,
            first_nearest_hits,
            prediction[:, :3],
            hits_indexes_global,
            first_hits_indexes,
            second_hits_indexes,
            cand_mask,
        )

        prediction = self.exec_model(chunk_data_x, 2)
        (
            first_dists,
            second_dists,
            first_nearest_hits,
            second_nearest_hits,
            first_hits_indexes,
            second_hits_indexes,
        ) = self.search_both(
            prediction, hits_by_station[2], indexes_by_station[2]
        )
        in_ellipse_mask1 = self.filter_hits_by_distance(prediction,
                                                        first_dists)
        in_ellipse_mask2 = self.filter_hits_by_distance(
            prediction[:, 4:], second_dists
        )
        chunk_data_x, hits_indexes_global, cand_mask = self.prolong(
            in_ellipse_mask1,
            in_ellipse_mask2,
            chunk_data_x,
            2,
            first_nearest_hits,
            prediction[:, :3],
            hits_indexes_global,
            first_hits_indexes,
            second_hits_indexes,
            cand_mask,
        )

        chunk_data_x[:, 2, :3][saved_mask] = saved_hits[saved_mask]
        hits_indexes_global[:, 2][saved_mask] = saved_indexes[saved_mask]
        cand_mask[saved_mask] = 1

        for stations_gone in range(3, max_n_stations):
            if stations_gone == 5:
                cand_mask, chunk_data_x, hits_indexes_global = self.drop_short(
                    cand_mask, chunk_data_x, hits_indexes_global
                )
            prediction = self.exec_model(chunk_data_x, stations_gone)

            (
                first_dists,
                second_dists,
                first_nearest_hits,
                second_nearest_hits,
                first_hits_indexes,
                second_hits_indexes,
            ) = self.search_both(
                prediction,
                hits_by_station[stations_gone],
                indexes_by_station[stations_gone],
            )

            in_ellipse_mask1 = self.filter_hits_by_distance(
                prediction, first_dists
            )
            in_ellipse_mask2 = self.filter_hits_by_distance(
                prediction[:, 4:], second_dists
            )
            chunk_data_x, hits_indexes_global, cand_mask = self.prolong(
                in_ellipse_mask1,
                in_ellipse_mask2,
                chunk_data_x,
                stations_gone,
                first_nearest_hits,
                prediction[:, :3],
                hits_indexes_global,
                first_hits_indexes,
                second_hits_indexes,
                cand_mask,
            )

        return hits_indexes_global

    def exec_model(self, inputs, stations_gone: int):
        outputs = torch.from_numpy(self.model.predict(inputs.numpy()))
        single_prediction = outputs[:, stations_gone - 1]
        return single_prediction

    def drop_short(self, cand_mask, chunk_data_x, hits_indexes_global):
        mask = cand_mask.to(torch.bool)
        chunk_data_x = chunk_data_x[mask].contiguous()
        hits_indexes_global = hits_indexes_global[mask]
        cand_mask = cand_mask[mask]
        return cand_mask, chunk_data_x, hits_indexes_global

    def drop_duplicated(
        self, cand_mask, chunk_data_x, hits_indexes_global, stations_gone: int
    ):
        duplicated = torch.logical_or(
            hits_indexes_global[:, stations_gone - 2]
            != hits_indexes_global[:, stations_gone - 1],
            hits_indexes_global[:, stations_gone - 1] == -1,
        )
        chunk_data_x = chunk_data_x[duplicated].contiguous()
        hits_indexes_global = hits_indexes_global[duplicated]
        cand_mask = cand_mask[duplicated]
        return cand_mask, chunk_data_x, hits_indexes_global

    def search_torchdist(self, centers, global_hits, global_indexes):
        centers = centers[:, :3]
        D, I = torch.min(
            torch.cdist(
                centers, global_hits, compute_mode="use_mm_for_euclid_dist"
            ),
            dim=1,
        )
        hits_indexes = global_indexes[I]
        nearest_hits = global_hits[I]
        return D, nearest_hits, hits_indexes

    def filter_hits_by_distance(self, ellipses, distances):
        return (distances.flatten() < ellipses[:, 3]).int()

    def prolong(
        self,
        in_ellipse_mask1,
        in_ellipse_mask2,
        chunk_data_x,
        stations_gone: int,
        first_nearest_hits,
        second_nearest_hits,
        hits_indexes_global,
        first_hits_indexes,
        second_hits_indexes,
        cand_mask,
    ):
        not_ellipse_mask = ~in_ellipse_mask1.to(torch.bool)
        first_nearest_hits[not_ellipse_mask] = second_nearest_hits[
            not_ellipse_mask
        ]
        first_hits_indexes[not_ellipse_mask] = -1
        in_ellipse_mask1[not_ellipse_mask] = in_ellipse_mask2[not_ellipse_mask]

        cand_mask = in_ellipse_mask1 * cand_mask
        chunk_data_x[:, stations_gone, :3] = (
            first_nearest_hits * cand_mask.reshape(-1, 1)
        )
        hits_indexes_global[:, stations_gone] = (
            first_hits_indexes * cand_mask + cand_mask - 1
        )
        return chunk_data_x, hits_indexes_global, cand_mask

    def prolong_single(
        self,
        in_ellipse_mask,
        chunk_data_x,
        stations_gone: int,
        nearest_hits,
        hits_indexes_global,
        hits_indexes,
        cand_mask,
    ):
        cand_mask = in_ellipse_mask * cand_mask
        chunk_data_x[:, stations_gone, :3] = nearest_hits * cand_mask.reshape(
            -1, 1
        )
        hits_indexes_global[:, stations_gone] = (
            hits_indexes * cand_mask + cand_mask - 1
        )
        return chunk_data_x, hits_indexes_global, cand_mask

    def search_both(self, centers, global_hits, global_indexes):
        flattened = centers.reshape((-1, 4))
        flattened = flattened[:, :3]
        D, I = torch.min(
            torch.cdist(
                flattened, global_hits, compute_mode="use_mm_for_euclid_dist"
            ),
            dim=1,
        )
        dist = D.flatten().reshape(-1, 2)
        indexes = I.reshape((-1, 2))
        first_indexes = indexes[:, 0]
        second_indexes = indexes[:, 1]

        first_hits_indexes = global_indexes[first_indexes]
        first_nearest_hits = global_hits[first_indexes]
        second_hits_indexes = global_indexes[second_indexes]
        second_nearest_hits = global_hits[second_indexes]
        first_dists = dist[:, 0]
        second_dists = dist[:, 1]
        return (
            first_dists,
            second_dists,
            first_nearest_hits,
            second_nearest_hits,
            first_hits_indexes,
            second_hits_indexes,
        )

    def search_single(self, centers, hits, indexes):
        centers = centers[:, :3]
        D, I = torch.min(
            torch.cdist(centers, hits, compute_mode="use_mm_for_euclid_dist"),
            dim=1,
        )
        dists = D.flatten()
        inds = I.flatten()

        nearest_hits = hits[inds]
        hits_indexes = indexes[inds]
        return dists, nearest_hits, hits_indexes


def build_df(indexes):
    new_columns = [*[f"hit_id_{i}" for i in range(35)], "event", "track_pred"]
    if len(indexes) == 0:
        return pd.DataFrame(columns=new_columns).astype(float)
    indexes = indexes.cpu().numpy()

    res_df = pd.DataFrame(
        data=indexes, columns=[f"hit_id_{i}" for i in range(35)]
    )
    res_df["track_pred"] = True
    res_df["track_pred"] = res_df["track_pred"].astype(bool)
    return res_df


def get_seeds_only_first_station(df, columns=["x", "y", "z"]):
    real = df
    temp1 = real[real.station == 0]
    st0_hits = temp1[columns].values

    seeds = torch.zeros(
        (len(temp1), N_STATIONS, 3), dtype=torch.float32, device=DEVICE
    )
    seeds[:, 0, :] = torch.from_numpy(st0_hits)
    return seeds


def get_first_station_hits(df, columns=["x", "y", "z"]):
    real = df
    temp1 = real[real.station == 0]
    st0_hits = temp1[columns].values
    return st0_hits


def build_hits(target_df, cols=["x", "y", "z"]):
    cont = (
        torch.from_numpy(target_df[cols].values)
        .contiguous()
        .to(torch.float32)
        .to(DEVICE)
    )
    return cont


def parse_df(df, cols=["x", "y", "z"]):
    first_hits = get_first_station_hits(df)
    first_indexes = df[df.station == 0].index.values

    hits_by_station = []
    indexes_by_station = []
    for station in range(N_STATIONS):
        station_df = df.query("station == @station or station == @station + 1")
        hits_by_station.append(
            station_df[["x", "y", "z"]].values.astype("float32")
        )
        indexes_by_station.append(station_df.index.values)
    return first_hits, first_indexes, hits_by_station, indexes_by_station


def setup(first_hits, first_indexes, hits_by_station, indexes_by_station):
    chunk_data_x = torch.zeros(
        (len(first_hits), N_STATIONS, 3), dtype=torch.float32, device=DEVICE
    )
    chunk_data_x[:, 0, :3] = torch.from_numpy(first_hits)

    cand_mask = torch.ones(len(chunk_data_x), dtype=torch.int64, device=DEVICE)
    hits_indexes_global = torch.full(
        (len(chunk_data_x), 35), -1, dtype=torch.int64, device=DEVICE
    )
    hits_indexes_global[:, 0] = torch.from_numpy(first_indexes)

    hits_by_station = [
        torch.from_numpy(hits).to(DEVICE) for hits in hits_by_station
    ]
    indexes_by_station = [
        torch.from_numpy(indexes).to(DEVICE) for indexes in indexes_by_station
    ]

    return (
        chunk_data_x,
        cand_mask,
        hits_indexes_global,
        hits_by_station,
        indexes_by_station,
    )
